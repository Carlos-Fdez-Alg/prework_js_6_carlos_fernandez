const juego = () => {
    let jugados = 0;
    let ganados = 0;
    let perdidos = 0;

    let partidas = prompt('Indica el número de partidas que deseas jugar');
    if (partidas <= 0) {
        alert('Introduce un número positivo por favor');
    } else {
        while (jugados < partidas) { 
            let jugada = prompt('Escribe "piedra", "papel" o "tijera"'); //Introducir comillas dobles y simples me permite evitar los \
            jugada = jugada.toLowerCase(); //Lo paso a minúsculas por si el usuario teclea la primera letra en mayúscula
            let aleatorio = Math.floor(Math.random()*3); //Genero un número aleatorio: 0, 1 o 2
            let elige = '';
            switch(aleatorio) {
                case 0:
                    elige = 'piedra';
                    break;
                case 1:
                    elige = 'papel';
                    break;
                case 2:
                    elige = 'tijera';
                    break;
            }
        
            if (jugada == 'piedra' && elige == 'piedra'){
                alert(`${jugada} - ${elige}: Empate`);
            } else if (jugada == 'piedra' && elige == 'papel'){
                alert(`${jugada} - ${elige}: Gana PC`);
                perdidos++;
            } else if (jugada == 'piedra' && elige == 'tijera'){
                alert(`${jugada} - ${elige}: Gana Jugador`);
                ganados++;
            } else if (jugada == 'tijera' && elige == 'tijera'){
                alert(`${jugada} - ${elige}: Empate`);
            } else if (jugada == 'tijera' && elige == 'piedra'){
                alert(`${jugada} - ${elige}: Gana PC`);
                perdidos++;
            } else if (jugada == 'tijera' && elige == 'papel'){
                alert(`${jugada} - ${elige}: Gana Jugador`);
                ganados++;
            } else if (jugada == 'papel' && elige == 'papel'){
                alert(`${jugada} - ${elige}: Empate`);
            } else if (jugada == 'papel' && elige == 'tijera'){
                alert(`${jugada} - ${elige}: Gana PC`);
                perdidos++;
            } else if (jugada == 'papel' && elige == 'piedra'){
                alert(`${jugada} - ${elige}: Gana Jugador`);
                ganados++;
            }
        
            jugados++;
        }
        
        document.write("Partidas jugadas: "+jugados+"<br>");
        document.write("Partidas ganadas: "+ganados+"<br>");
        document.write("Partidas perdidas: "+perdidos+"<br>");
        document.write("Partidas empatadas: "+(jugados-ganados-perdidos));
    }    
}